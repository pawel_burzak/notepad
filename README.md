# Notepad RESTful Api #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

#### This application manages and stores simple notes in database. It provides create, read, update and delete notes. Authorization and authentication are ignored.####

### How do I get set up? ###

#### For running the project you need: ####
* JDK 1.8 or later
* Maven 3 or later
* MySQL Server 5.7 or later and MySQL Workbench
* Lombok plugin in your IDE

#### Dependencies used in the project: ####
* Spring Boot
* Model Mapper
* H2 Database
* Project Lombok
* MySQL Connector
    
#### Database setup: ####
* Create new User with all privileges:  
	Login Name: root  
	Password: root  
* Create new Schema:  
	Name: notepad  

	You can run scripts in MySQL Workbench:  
	CREATE USER 'root'@'localhost' IDENTIFIED BY 'root';  
	GRANT ALL PRIVILEGES ON * . * TO 'root'@'localhost';  
	FLUSH PRIVILEGES;  
	CREATE SCHEMA notepad;  

#### Steps how to build and run the project: #####
* Connect to MySQL database in your IDE, for example instruction for IntelliJ Idea 2017.3: https://www.jetbrains.com/help/idea/connecting-to-a-database.html#mysql
* Run script in terminal in your IDE:  
	mvn spring-boot:run

### Example usages: ###
  
Notes contains fields:  
* id – Auto-generated number, forbidden  
* version – Version of Notes, forbidden  
* title – Title of Notes, required  
* content – Content of Notes, required  
* created – Date of initial creation, forbidden  
* modified – Date of last modification, forbidden  
  
#### List of Notes
* URL: /notepad/  
* Method: GET  
* URL Params: None  
* Data Params: None  
+ Success Response:  
     * Code: 200 OK,  
     * Content:  
        [{  
          "id": 1,  
          "version": 0,  
          "title": "title1",  
          "content": "content1",  
          "created": "2018-03-10 17:12:57.0",  
          "modified": "2018-03-10 18:24:14.0"  
        },  
        {  
          "id": 2,  
          "version": 0,  
          "title": "title2",  
          "content": "content2",  
          "created": "2018-03-10 17:14:09.0",  
          "modified": null  
        }]  
+ Error response:  
     * Code: 404 NOT FOUND  
+ Sample call:  
     * http://localhost:8080/notepad/  
      
#### Show Notes
* URL: /notepad/:id  
* Method: GET  
* URL Params: id=[long]  
* Data Params: None  
+ Success Response:  
     * Code: 200 OK,  
     * Content:  
        {  
          "id": 1,  
          "version": 0,  
          "title": "title1",  
          "content": "content1",  
          "created": "2018-03-10 17:12:57.0",  
          "modified": "2018-03-10 18:24:14.0"  
        }  
+ Error response:  
     * Code: 404 NOT FOUND  
+ Sample call:  
     * http://localhost:8080/notepad/1  
      
#### Create Notes
* URL: /notepad/  
* Method: POST  
* URL Params: None  
+ Data Params:  
     * JSON (application/json)  
      {  
    	"title" : "title",  
    	"content" : "content"  
      }  
+ Success Response:  
     * Code: 201 CREATED,  
     * Headers: Location -> /notepad/1  
+ Error response:  
     * Code: 400 BAD REQUEST  
+ Sample call:  
     * http://localhost:8080/notepad/  
     * JSON (application/json)  
      {  
	    "title" : "title",  
	    "content" : "content"  
      }  
      
#### Update Notes
* URL: /notepad/:id  
* Method: PUT  
* URL Params: id=[long]  
+ Data Params:  
     * JSON (application/json)  
      {  
	    "title" : "updated title",  
	    "content" : "updated content"  
      }  
+ Success Response:  
     * Code: 200 OK  
+ Error response:  
     * Code: 400 BAD REQUEST  
      OR  
     * Code: 404 NOT FOUND  
+ Sample call:  
     * http://localhost:8080/notepad/1  
     * JSON (application/json)  
      {  
	    "title" : "updated title",  
	    "content" : "updated content"  
      }  
      
#### Delete Notes
* URL: /notepad/:id  
* Method: DELETE  
* URL Params: id=[long]  
* Data Params: None  
+ Success Response:  
     * Code: 200 OK  
+ Error response:  
     * Code: 404 NOT FOUND  
+ Sample call:  
     * http://localhost:8080/notepad/1  
	  
package pl.burzak.pawel.notepad.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.validation.BindingResult;
import pl.burzak.pawel.notepad.domain.Notes;
import pl.burzak.pawel.notepad.dto.NotesDto;
import pl.burzak.pawel.notepad.exception.NotesNotFoundException;
import pl.burzak.pawel.notepad.service.NotesService;

import java.util.ArrayList;
import java.util.Arrays;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@WebMvcTest(NotesController.class)
public class NotesControllerTest {

    public static final Long NOTES_ID = 1L;
    public static final Long NOTES_VERSION = 0L;

    @Autowired
    private MockMvc mvc;

    @MockBean
    private NotesService notesService;

    private Notes notes;

    private NotesDto notesDto;

    private ModelMapper mockModelMapper;

    private BindingResult mockBindingResult;

    @Before
    public void setUp() {
        notes = buildNotes("test title", "test content");
        notesDto = buildNotesDto("test title", "test content");
        mockModelMapper = Mockito.mock(ModelMapper.class);
        mockBindingResult = mock(BindingResult.class);
    }

    @Test
    public void testMockCreation() {
        assertNotNull(notesService);
        assertNotNull(mvc);
    }

    @Test
    public void shouldReturnNotesDtoWithOkStatusWhenGetAndIdIsFound() throws Exception {
        //given
        notes.setId(NOTES_ID);
        notesDto.setId(NOTES_ID);

        Mockito.when(notesService.findById(NOTES_ID)).thenReturn(notes);
        Mockito.when(mockModelMapper.map(notes, NotesDto.class)).thenReturn(notesDto);

        //when then
        mvc.perform(get("/{id}", 1)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(1)))
                .andExpect(jsonPath("$.title", is("test title")))
                .andExpect(jsonPath("$.content", is("test content")));

        verify(notesService, times(1)).findById(NOTES_ID);
    }

    @Test
    public void shouldReturnStatusNotFoundWhenGetAndIdIsNotFound() throws Exception {
        //given
        Mockito.when(notesService.findById(NOTES_ID)).thenThrow(NotesNotFoundException.class);

        //when then
        mvc.perform(get("/{id}", 1)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());

        verify(notesService, times(1)).findById(NOTES_ID);
    }

    @Test
    public void shouldReturnAllNotesDtoWithOkStatusWhenGetAndNotesAreFound() throws Exception {
        //given
        notes.setId(NOTES_ID);
        notesDto.setId(NOTES_ID);
        Notes notes2 = new Notes();
        notes2.setId(2L);
        notes2.setTitle("test title 2");
        notes2.setContent("test content 2");
        NotesDto notesDto2 = new NotesDto();
        notesDto2.setId(2L);
        notesDto2.setTitle("test title 2");
        notesDto2.setContent("test content 2");

        Mockito.when(notesService.findAll()).thenReturn(Arrays.asList(notes, notes2));
        Mockito.when(mockModelMapper.map(notes, NotesDto.class)).thenReturn(notesDto);
        Mockito.when(mockModelMapper.map(notes2, NotesDto.class)).thenReturn(notesDto2);

        //when then
        mvc.perform(get("/")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].id", is(1)))
                .andExpect(jsonPath("$[0].title", is("test title")))
                .andExpect(jsonPath("$[0].content", is("test content")))
                .andExpect(jsonPath("$[1].id", is(2)))
                .andExpect(jsonPath("$[1].title", is("test title 2")))
                .andExpect(jsonPath("$[1].content", is("test content 2")));

        verify(notesService, times(1)).findAll();
    }

    @Test
    public void shouldReturnStatusNotFoundWhenGetAndNotesAreNotFound() throws Exception {
        //given
        Mockito.when(notesService.findAll()).thenReturn(new ArrayList<>());

        //when then
        mvc.perform(get("/")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());

        verify(notesService, times(1)).findAll();
    }

    @Test
    public void shouldReturnHeaderWithStatusCreatedWhenPostAndNotesDtoIsValid() throws Exception {
        //given
        Mockito.when(mockBindingResult.hasErrors()).thenReturn(false);
        Mockito.when(mockModelMapper.map(notesDto, Notes.class)).thenReturn(notes);
        Mockito.when(notesService.create(notes)).thenReturn(NOTES_ID);

        //when then
        mvc.perform(post("/")
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(notesDto)))
                .andExpect(status().isCreated())
                .andExpect(header().string("location", containsString("http://localhost/1")));

        verify(notesService, times(1)).create(notes);
    }

    @Test
    public void shouldReturnStatusBadRequestWhenPostAndNotesDtoIsNotValid() throws Exception {
        //given
        notesDto.setId(NOTES_ID);
        notesDto.setVersion(NOTES_VERSION);

        Mockito.when(mockBindingResult.hasErrors()).thenReturn(true);

        //when then
        mvc.perform(post("/")
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(notesDto)))
                .andExpect(status().isBadRequest());

        verify(notesService, times(0)).create(new Notes());
    }

    @Test
    public void shouldReturnStatusOkWhenPutAndNotesDtoIsValid() throws Exception {
        //given
        notes.setId(NOTES_ID);

        Mockito.when(mockBindingResult.hasErrors()).thenReturn(false);
        Mockito.when(mockModelMapper.map(notesDto, Notes.class)).thenReturn(notes);
        Mockito.doNothing().when(notesService).update(notes);

        //when then
        mvc.perform(put("/{id}", 1)
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(notesDto)))
                .andExpect(status().isOk());

        verify(notesService, times(1)).update(notes);
    }

    @Test
    public void shouldReturnStatusBadRequestWhenPutAndNotesDtoIsNotValid() throws Exception {
        //given
        notesDto.setId(NOTES_ID);
        notesDto.setVersion(NOTES_VERSION);

        Mockito.when(mockBindingResult.hasErrors()).thenReturn(true);

        //when then
        mvc.perform(post("/")
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(notesDto)))
                .andExpect(status().isBadRequest());

        verify(notesService, times(0)).update(new Notes());
    }

    @Test
    public void shouldReturnStatusNotFoundWhenPutAndIdIsNotFound() throws Exception {
        //given
        notes.setId(NOTES_ID);

        Mockito.when(mockBindingResult.hasErrors()).thenReturn(false);
        Mockito.when(mockModelMapper.map(notesDto, Notes.class)).thenReturn(notes);
        Mockito.doThrow(NotesNotFoundException.class).when(notesService).update(notes);

        //when then
        mvc.perform(put("/{id}", 1)
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(notesDto)))
                .andExpect(status().isNotFound());

        verify(notesService, times(1)).update(notes);
    }

    @Test
    public void shouldReturnStatusOkWhenDeleteAndIdIsFound() throws Exception {
        //when then
        mvc.perform(delete("/{id}", 1)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        verify(notesService, times(1)).delete(NOTES_ID);
    }

    @Test
    public void shouldReturnStatusNotFoundWhenDeleteAndIdIsNotFound() throws Exception {
        //given
        Mockito.doThrow(NotesNotFoundException.class).when(notesService).delete(NOTES_ID);

        //when then
        mvc.perform(delete("/{id}", 1)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());

        verify(notesService, times(1)).delete(NOTES_ID);
    }

    private static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private NotesDto buildNotesDto(String testTitle, String testContent) {
        notesDto = new NotesDto();
        notesDto.setTitle(testTitle);
        notesDto.setContent(testContent);
        return notesDto;
    }

    private Notes buildNotes(String testTitle, String testContent) {
        notes = new Notes();
        notes.setTitle(testTitle);
        notes.setContent(testContent);
        return notes;
    }
}
package pl.burzak.pawel.notepad.service;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import pl.burzak.pawel.notepad.domain.Notes;
import pl.burzak.pawel.notepad.exception.NotesNotFoundException;
import pl.burzak.pawel.notepad.repository.NotesRepository;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class NotesServiceTest {

    public static final Long NOTES_ID = 1L;

    @Autowired
    private NotesService notesService;

    @MockBean
    private NotesRepository notesRepository;

    private Notes notes;

    @Before
    public void setUp() {
        notes = new Notes();
        notes.setId(NOTES_ID);
    }

    @Test
    public void testMockCreation() {
        assertNotNull(notesRepository);
        assertNotNull(notesService);
    }

    @Test
    public void shouldReturnLong1LWhenCreateFirstNotes() {
        //given
        Mockito.when(notesRepository.save(notes)).thenReturn(notes);

        //when
        Long result = notesService.create(notes);

        //then
        assertEquals(notes.getId(), result);
    }

    @Test
    public void shouldReturnAllNotesWhenFindAll() {
        //given
        List<Notes> notesList = Arrays.asList(notes, new Notes());

        Mockito.when(notesRepository.findAll()).thenReturn(notesList);

        //when
        List<Notes> result = notesService.findAll();

        //then
        assertEquals(notesList.size(), result.size());
    }

    @Test
    public void shouldReturnNotesWhenFindByIdAndIdIsFound() {
        //given
        Mockito.when(notesRepository.findById(NOTES_ID)).thenReturn(Optional.of(notes));

        //when
        Notes result = notesService.findById(NOTES_ID);

        //then
        assertEquals(notes, result);
    }

    @Test(expected = NotesNotFoundException.class)
    public void shouldThrowExceptionWhenFindByIdAndIdIsNotFound() throws NotesNotFoundException {
        //given
        Mockito.when(notesRepository.findById(2L)).thenReturn(Optional.empty());

        //when
        Notes result = null;
        try {
            result = notesService.findById(2L);

            //then
        } catch (NotesNotFoundException ex) {
            assertNull(result);
            throw ex;
        }
    }

    @Test
    public void shouldUpdateNotesWhenNotesIsFound() {
        //given
        notes.setTitle("test title");
        notes.setContent("test content");

        Notes updatedNotes = new Notes();
        updatedNotes.setId(NOTES_ID);
        updatedNotes.setTitle("updated title");
        updatedNotes.setContent("updated content");

        Mockito.when(notesRepository.findById(NOTES_ID)).thenReturn(Optional.of(notes));

        //when
        notesService.update(updatedNotes);
        Notes result = notesService.findById(NOTES_ID);

        //then
        assertEquals(Long.valueOf(NOTES_ID), result.getId());
        assertEquals("updated title", result.getTitle());
        assertEquals("updated content", result.getContent());
    }

    @Test(expected = NotesNotFoundException.class)
    public void shouldDeleteNotesWhenNotesIsFound() throws NotesNotFoundException {
        //given
        Mockito.when(notesRepository.findById(NOTES_ID)).thenReturn(Optional.of(notes));

        //when
        Notes test = notesService.findById(NOTES_ID);
        assertNotNull(test);

        notesService.delete(NOTES_ID);
        Mockito.doNothing().when(notesRepository).delete(NOTES_ID);
        Mockito.when(notesRepository.findById(NOTES_ID)).thenReturn(Optional.empty());

        Notes result = null;
        try {
            result = notesService.findById(NOTES_ID);

            //then
        } catch (NotesNotFoundException ex) {
            assertNull(result);
            throw ex;
        }
    }
}
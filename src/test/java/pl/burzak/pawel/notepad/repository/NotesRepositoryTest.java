package pl.burzak.pawel.notepad.repository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;
import pl.burzak.pawel.notepad.domain.Notes;

import java.util.Date;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

@RunWith(SpringRunner.class)
@DataJpaTest
public class NotesRepositoryTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private NotesRepository notesRepository;

    private Notes notes;

    @Before
    public void setUp() {
        notes = new Notes();
        notes.setTitle("test title");
        notes.setContent("test content");
    }

    @Test
    public void testMockCreation() {
        assertNotNull(entityManager);
        assertNotNull(notesRepository);
    }

    @Test
    public void shouldReturnOptionalNotesWhenFindByIdAndIdIsFound() {
        // given
        entityManager.persist(notes);
        entityManager.flush();

        // when
        Optional<Notes> dbNotes = notesRepository.findById(notes.getId());

        // then
        assertEquals(notes, dbNotes.get());
        assertEquals(notes.getId(), dbNotes.get().getId());
    }

    @Test
    public void shouldReturnOptionalEmptyWhenFindByIdAndIdIsNotFound() {
        // given
        Optional<Notes> optionalNotes = Optional.empty();

        // when
        Optional<Notes> dbNotes = notesRepository.findById(100L);

        // then
        assertEquals(!optionalNotes.isPresent(), !dbNotes.isPresent());
    }

    @Test
    public void shouldReturnNotesWhenSave() {
        // given
        Date createdDate = new Date();
        entityManager.persist(notes);
        entityManager.flush();

        // when
        Notes dbNotes = notesRepository.save(notes);

        // then
        assertEquals(notes, dbNotes);
        assertEquals(notes.getId(), dbNotes.getId());
        assertEquals("test title", dbNotes.getTitle());
        assertEquals("test content", dbNotes.getContent());
        assertEquals(createdDate.toString(), dbNotes.getCreated().toString());
        assertNull(dbNotes.getModified());
    }

    @Test
    public void shouldSetModifiedDateWhenUpdate() {
        // given
        Date createdDate = new Date();
        entityManager.persist(notes);
        entityManager.flush();

        // when
        notes.setTitle("updated title");
        notes.setContent("updated content");
        Date modifiedDate = new Date();
        entityManager.merge(notes);
        entityManager.flush();

        Optional<Notes> dbNotes = notesRepository.findById(notes.getId());

        // then
        assertEquals(notes.getId(), dbNotes.get().getId());
        assertEquals("updated title", dbNotes.get().getTitle());
        assertEquals("updated content", dbNotes.get().getContent());
        assertEquals(createdDate.toString(), dbNotes.get().getCreated().toString());
        assertEquals(modifiedDate.toString(), dbNotes.get().getModified().toString());
    }
}
package pl.burzak.pawel.notepad.exception;

/**
 * NotesNotFoundException is thrown when Notes is not found in database.
 *
 * @author Burzak Paweł
 */
public class NotesNotFoundException extends RuntimeException {
    private static final long serialVersionUID = 3250849363274539929L;
}

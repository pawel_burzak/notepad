package pl.burzak.pawel.notepad.rest;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;
import pl.burzak.pawel.notepad.domain.Notes;
import pl.burzak.pawel.notepad.dto.NotesDto;
import pl.burzak.pawel.notepad.exception.NotesNotFoundException;
import pl.burzak.pawel.notepad.service.NotesService;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Rest controller communicates between client and application using Http protocol.
 *
 * @author Burzak Paweł
 */
@RestController
public class NotesController {

    private final NotesService notesService;
    private final ModelMapper modelMapper;

    @Autowired
    public NotesController(NotesService notesService,
                           ModelMapper modelMapper) {
        this.notesService = notesService;
        this.modelMapper = modelMapper;
    }

    /**
     * This method validates NotesDto, converts NotesDto to Notes and transfers Notes to
     * service layer to persist it in database.
     * @param notesDto Request object from Http protocol.
     * @param result BindingResult validates request object and add any errors.
     * @param ucBuilder UriComponentsBuilder builds Uri to return headers.
     * @return If succeed headers with http status created, otherwise http status bad request.
     */
    @PostMapping("/")
    public ResponseEntity<Void> create(@Valid @RequestBody NotesDto notesDto,
                                       BindingResult result,
                                       UriComponentsBuilder ucBuilder) {
        if (result.hasErrors()) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        Long id = notesService.create(convertToEntity(notesDto));
        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(ucBuilder.path("/{id}").buildAndExpand(id).toUri());
        return new ResponseEntity(headers, HttpStatus.CREATED);
    }

    /**
     * This method gets object Notes with requested id from service layer, converts Notes to NotesDto and
     * send NotesDto to client.
     * @param id Request parameter sent from http protocol to find Notes with id.
     * @return If succeed object NotesDto with http status ok, otherwise http status not found.
     */
    @GetMapping("/{id}")
    public ResponseEntity<NotesDto> get(@PathVariable("id") Long id) {
        try {
            NotesDto notesDto = convertToDto(notesService.findById(id));
            return new ResponseEntity<>(notesDto, HttpStatus.OK);
        } catch (NotesNotFoundException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * This method gets list of Notes from service layer, converts Notes to NotesDto and
     * send list of NotesDto to client.
     * @return If succeed list of NotesDto with http status ok, otherwise empty list with http status not found.
     */
    @GetMapping("/")
    public ResponseEntity<List<NotesDto>> getAll() {
        List<NotesDto> allNotesDto = notesService.findAll().stream()
                .map(notes -> convertToDto(notes))
                .collect(Collectors.toList());

        return new ResponseEntity<>(allNotesDto, allNotesDto.isEmpty() ? HttpStatus.NOT_FOUND : HttpStatus.OK);
    }

    /**
     * This method validates NotesDto, converts NotesDto to Notes and transfers Notes to
     * service layer to merge it in database.
     * @param notesDto Request object from http protocol.
     * @param result BindingResult validates request object and add any errors.
     * @return If succeed http status ok, if result has errors http status bad request,
     * otherwise http status not found.
     */
    @PutMapping("/{id}")
    public ResponseEntity<Void> update(@PathVariable("id") Long id,
                                       @Valid @RequestBody NotesDto notesDto,
                                       BindingResult result) {
        if (result.hasErrors()) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        try {
            Notes notes = convertToEntity(notesDto);
            notes.setId(id);
            notesService.update(notes);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (NotesNotFoundException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * This method sends requested id to service layer to delete Notes with id.
     * @param id Request parameter sent from http protocol to delete Notes with id.
     * @return If succeed http status ok, otherwise http status not found.
     */
    @DeleteMapping("/{id}")
    public ResponseEntity<Void> delete(@PathVariable("id") Long id) {
        try {
            notesService.delete(id);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (NotesNotFoundException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * This method maps Notes to NotesDto using ModelMapper.
     * @param notes Source object.
     * @return Destination object - NotesDto.
     */
    private NotesDto convertToDto(Notes notes) {
        return modelMapper.map(notes, NotesDto.class);
    }

    /**
     * This method maps NotesDto to Notes using ModelMapper.
     * @param notesDto Source object.
     * @return Destination object - Notes.
     */
    private Notes convertToEntity(NotesDto notesDto) {
        return modelMapper.map(notesDto, Notes.class);
    }

}

package pl.burzak.pawel.notepad.domain;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * This is entity Notes.
 *
 * @author Burzak Paweł
 */
@NoArgsConstructor
@Getter
@Setter
@EqualsAndHashCode
@Entity
@Table(name = "NOTES")
public class Notes implements Serializable {
    private static final long serialVersionUID = -5856367145130097725L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;

    @Version
    @Column(name = "LATEST_VERSION")
    private Long version;

    @Column(name = "TITLE")
    private String title;

    @Column(name = "CONTENT")
    private String content;

    @Column(name = "CREATED")
    private Date created;

    @Column(name = "MODIFIED")
    private Date modified;

    @PrePersist
    public void setCreationDate() {
        this.created = new Date();
    }

    @PreUpdate
    public void setModificationDate() {
        this.modified = new Date();
    }

}


package pl.burzak.pawel.notepad.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.burzak.pawel.notepad.domain.Notes;
import pl.burzak.pawel.notepad.exception.NotesNotFoundException;
import pl.burzak.pawel.notepad.repository.NotesRepository;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

/**
 * NotesService communicates with persistence layer.
 *
 * @author Burzak Paweł
 */
@Service
@Transactional
public class NotesService {

    private final NotesRepository notesRepository;

    @Autowired
    public NotesService(NotesRepository notesRepository) {
        this.notesRepository = notesRepository;
    }

    /**
     * This method sends Notes to persistence layer to persist it in database.
     * @param notes Object to persist in database.
     * @return Notes id.
     */
    public Long create(Notes notes) {
        notesRepository.save(notes);
        return notes.getId();
    }

    /**
     * This method gets Notes with requested id from persistence layer.
     * @param id Request parameter to find Notes with id.
     * @return If succeed object Notes, otherwise throws NotesNotFoundException.
     */
    public Notes findById(Long id) {
        Optional<Notes> dbNotes = notesRepository.findById(id);

        if (!dbNotes.isPresent()) {
            throw new NotesNotFoundException();
        }

        return dbNotes.get();
    }

    /**
     * This method gets list of Notes from persistence layer.
     * @return List of Notes.
     */
    public List<Notes> findAll() {
        return notesRepository.findAll();
    }

    /**
     * This method gets Notes from persistence layer and sets all fields from requested Notes.
     * @param notes Requested Notes.
     */
    public void update(Notes notes) {
        Optional<Notes> dbNotes = notesRepository.findById(notes.getId());

        if (!dbNotes.isPresent()) {
            throw new NotesNotFoundException();
        }

        dbNotes.get().setTitle(notes.getTitle());
        dbNotes.get().setContent(notes.getContent());
    }

    /**
     * This method gets Notes from persistence layer with requested id
     * and invoke delete method with requested id on persistence layer to delete object.
     * @param id Request parameter to find and delete Notes with id.
     */
    public void delete(Long id) {
        Optional<Notes> dbNotes = notesRepository.findById(id);

        if (!dbNotes.isPresent()) {
            throw new NotesNotFoundException();
        }

        notesRepository.delete(id);
    }
}

package pl.burzak.pawel.notepad.dto;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Null;
import java.io.Serializable;

/**
 * This is NotesDto to transfer objects from persistence layer to web layer.
 *
 * @author Burzak Paweł
 */
@NoArgsConstructor
@Getter
@Setter
@EqualsAndHashCode
public class NotesDto implements Serializable {
    private static final long serialVersionUID = -8552657859932425438L;

    @Null
    private Long id;

    @Null
    private Long version;

    @NotBlank
    private String title;

    @NotBlank
    private String content;

    @Null
    private String created;

    @Null
    private String modified;
}

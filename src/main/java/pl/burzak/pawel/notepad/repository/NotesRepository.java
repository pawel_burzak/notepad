package pl.burzak.pawel.notepad.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import pl.burzak.pawel.notepad.domain.Notes;

import java.util.Optional;

/**
 * NotesRepository is data access object.
 *
 * @author Burzak Paweł
 */
@Repository
public interface NotesRepository extends JpaRepository<Notes, Long> {

    /**
     * This method finds Optional of Notes in database.
     * @param id Criteria of searching in database.
     * @return Optional of Notes or Optional empty.
     */
    Optional<Notes> findById(@Param("id") Long id);
}
